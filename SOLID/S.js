// Single resposibility principle (Single class/entity = 1 task)

// ------First example------

// ---Bad approach---
// class User {
//   constructor(username, password) {
//     this.id = Math.random();
//     this.username = username;
//     this.password = password;
//   }

//   saveToDatabase() {
//     console.log('Saving to Database...');
//   }

//   log() {
//     console.log(this);
//   }

//   send() {
//     console.log('Sending data...');
//   }
// }

// const user = new User('Alex', '111');
// console.log(user.send());

// ---Good approach---
// class User {
//   constructor(username, password) {
//     this.id = Math.random();
//     this.username = username;
//     this.password = password;
//   }
// }

// class Database {
//   save(user) {
//     console.log('Saving to Database...');
//   }
// }

// class Logger {
//   log(user) {
//     console.log(user);
//   }
// }

// class UserController {
//   send(user) {
//     console.log('Sending data...');
//   }
// }

// ------Second example------

// ---Bad approach---
class DataFetcher {
  get() {}
  post() {}
  put() {}
  delete() {}

  getUser(id) {
    this.get(`http://localhost:8080/users/${id}`);
  }

  getRequisites(id) {
    this.get(`http://localhost:8080/users/requisites/${id}`);
  }

  getUsers() {
    this.get('http://localhost:8080/users');
  }
}

// ---Good approach---
class HttpClient {
  get() {}
  post() {}
  put() {}
  delete() {}
}

class UserService {
  constructor(client) {
    this.client = client;
  }

  getUser(id) {
    this.client.get(`http://localhost:8080/users/${id}`);
  }

  getUsers() {
    this.client.get('http://localhost:8080/users');
  }
}

class RequisiteService {
  constructor(client) {
    this.client = client;
  }

  createRequisite(data) {
    this.client.post(`http://localhost:8080/requisites`, data);
  }
  getRequisite(id) {
    this.client.get(`http://localhost:8080/requisites/${id}`);
  }
  updateRequisite(id, data) {
    this.client.update(`http://localhost:8080/requisites/${id}`, data);
  }
}

//----------------------------