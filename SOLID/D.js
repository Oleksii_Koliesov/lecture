// Dependency inversion principle

// ---Bad approach---

// class YandexMusicApi {
//   get() {}
// }

// class SpotifyApi {
//   findAll() {}
// }

// class VKMusicApi {
//   findAll() {}
// }

// const MusicApp = () => {
//   const API = YandexMusicApi();

//   API.get();
// };

// ---Good approach---

class MusicApi {
  getTracks() {}
}

class YandexMusicApi extends MusicApi {
  getTracks() {}
}

class SpotifyApi extends MusicApi {
  getTracks() {}
}

class VKMusicApi extends MusicApi {
  getTracks() {}
}

class MusicClient extends MusicApi {
  constructor(client) {
    this.client = client;
  }

  getTracks() {
    this.client.getTracks();
  }
}

const MusicApp = () => {
  const API = new MusicClient(new SpotifyApi());

  API.getTracks();
};
