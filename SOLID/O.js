// Open-closed principle (open to extension but closed to changes)

class Weapon {
  constructor(type, damage, range) {
    this.type = type;
    this.damage = damage;
    this.range = range;
  }

  attack() {
    // if (this.type === 'sword') {
    //   return console.log(`You attacked ${this.damage} damage with sword`);
    // }
    // if (this.type === 'crossbow') {
    //   return console.log(`You attacked ${this.damage} damage with crossbow`);
    // }
    console.log(`You attacked ${this.damage} damage with sword`);
  }
}

class Character {
  constructor(name, weapon) {
    this.name = name;
    this.weapon = weapon;
  }

  changeWeapon(newWeapon) {
    this.weapon = newWeapon;
  }

  attack() {
    this.weapon.attack();
  }
}

class Sword extends Weapon {
  constructor(damage, range) {
    super('sword', damage, range);
  }

  attack() {
    console.log(`You attacked ${this.damage} damage with a sword`);
  }
}

class Crossbow extends Weapon {
  constructor(damage, range) {
    super('crossbow', damage, range);
  }

  attack() {
    console.log(`You attacked ${this.damage} damage with a crossbow`);
  }
}

class Knife extends Weapon {
  constructor(damage, range) {
    super(damage, range);
  }

  attack() {
    console.log(`You attacked ${this.damage} damage with a knife`);
  }
}

const sword = new Sword(15, 2);
const character = new Character('Warrior', sword);
character.attack();

const crossbow = new Crossbow(40, 100);
character.changeWeapon(crossbow);
character.attack();

// ----Second Example----

class Person {
  constructor(fullname) {
    this.fullname = fullname;
  }
}

class PersonList {
  constructor() {
    this.persons = [];
  }

  sort() {
    // if (this.sort.length < 10) {
    //   BubbleSort.sort(this.persons);
    // } else if (this.sort.length < 1000) {
    //   MergeSort.sort(this.persons);
    // } else {
    //   QuickSort.sort(this.persons);
    // }
    SortClient.sort(this.persons);
  }
}

class Music {}

class MusicList {
  constructor() {
    this.musics = [];
  }

  sort() {
    SortClient.sort(this.musics);
  }
}

class Sort {
  static sort(arr) {}
}

class BubbleSort extends Sort {
  static sort(arr) {
    return arr;
  }
}

class QuickSort extends Sort {
  static sort(arr) {
    return arr;
  }
}

class MergeSort extends Sort {
  static sort(arr) {
    return arr;
  }
}

class SortClient extends Sort {
  static sort(arr) {
    if (arr.length < 10) {
      BubbleSort.sort(arr);
    } else if (arr.length < 1000) {
      MergeSort.sort(arr);
    } else {
      QuickSort.sort(arr);
    }
  }
}
