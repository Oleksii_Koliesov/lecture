// // Interface segregation principle
// interface AllHandler {
//     eat: () => void;
//     breathe: () => void;
//     fly: () => void;
//   }
  
// interface EatAndBreatheHendler {
//   eat: () => void;
//   breathe: () => void;
// }

// interface FlyHendler {
//   fly: () => void;
// }

// abstract class Animal implements EatAndBreatheHendler{
//   abstract name: String;

//   abstract eat(): void;
//   abstract breathe(): void;
// }

// class Dog extends Animal {
//   name: String

//   constructor(name: String) {
//     super()
//     this.name = name;
//   }

//   eat() {
//     console.log('eating...')
//   }

//   breathe() {
//     console.log('breathing...')
//   }
// }

// class Bird extends Animal implements FlyHendler {
//   name: String

//   constructor(name: String) {
//     super()
//     this.name = name;
//   }

//   eat() {
//     console.log('eating...')
//   }

//   breathe() {
//     console.log('breathing...')
//   }

//   fly() {
//     console.log('flying...')
//   }
// }