//Lisov Substitution Principle

// ---Bad approach---

// class Database {
//   connect() {}
//   read() {}
//   write() {}

//   joinTables() {}
// }

// class mySQLDatabase extends Database {
//   connect() {}
//   read() {}
//   write() {}

//   joinTables() {}
// }

// class MongoDatabase extends Database {
//   connect() {}
//   read() {}
//   write() {}

//   joinTables() {
//     throw new Error('У mongoDB нет таблиц');
//   }
// }

// ---Good approach---

class Database {
  connect() {}
  read() {}
  write() {}
}

class SQLDatabase extends Database {
  connect() {}
  read() {}
  write() {}

  joinTables() {}
}

class NoSQLDatabase extends Database {
  connect() {}
  read() {}
  write() {}

  refToDocument() {}
}

class mySQLDatabase extends SQLDatabase {
  connect() {}
  read() {}
  write() {}

  joinTables() {}
}

class MongoDatabase extends NoSQLDatabase {
  connect() {}
  read() {}
  write() {}

  refToDocument() {}
  createIndex() {}
}
