function bmwProducer(kind) {
  return kind === 'sport' ? sportCarFactory : familyCarFactory;
}

function sportCarFactory() {
  return new Z4();
}

function familyCarFactory() {
  return new I3();
}

class Z4 {
  info() {
    return 'Z4 is a Sport car!';
  }
}

class I3 {
  info() {
    return 'i3 is a Family car!';
  }
}

//-------------------------
// class TailFactory {
//   constructor(props) {
//     this.tailLength = props.tailLength;
//   }
// }
// class TorsoFactory {
//   constructor(props) {
//     this.color = props.color;
//   }
// }
// class HeadFactory {
//   constructor(props) {
//     this.snoutLength = props.snoutLength;
//   }
// }

// class ReptilePartFactory {
//   constructor(type, props) {
//     if (type === 'tail') {
//       return new TailFactory(props);
//     }
//     if (type === 'torso') {
//       return new TorsoFactory(props);
//     }
//     if (type === 'head') {
//       return new HeadFactory(props);
//     }
//   }
// }

// let alligator = {};
// let alligatorProps = {
//   tailLength: 2.5,
//   color: 'green',
//   snoutLength: 1,
// };

// alligator.tail = new ReptilePartFactory('tail', alligatorProps);
// alligator.torso = new ReptilePartFactory('torso', alligatorProps);
// alligator.head = new ReptilePartFactory('head', alligatorProps);

// console.log(alligator);

let registeredPartFactories = {};
registeredPartFactories['tail'] = class TailFactory {
  constructor(props) {
    this.tailLength = props.tailLength;
  }
};
registeredPartFactories['torso'] = class TorsoFactory {
  constructor(props) {
    this.color = props.color;
  }
};
registeredPartFactories['head'] = class HeadFactory {
  constructor(props) {
    this.snoutLength = props.snoutLength;
  }
};

class ReptilePartFactory {
  constructor(type, props) {
    return new registeredPartFactories[type](props);
  }
}

let alligator = {};
let alligatorProps = {
  tailLength: 2.5,
  color: 'green',
  snoutLength: 1,
};

alligator.tail = new ReptilePartFactory('tail', alligatorProps);
alligator.torso = new ReptilePartFactory('torso', alligatorProps);
alligator.head = new ReptilePartFactory('head', alligatorProps);

console.log(alligator);
