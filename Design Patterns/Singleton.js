class FancyLogger {
  constructor() {
    if (FancyLogger.instance) {
      return FancyLogger.instance;
    }
    this.logs = [];
    FancyLogger.instance = this;
  }

  log(message) {
    this.logs.push(message);
    console.log(`FANCY: ${message}`);
  }

  printLogCount() {
    console.log(`${this.logs.length} Logs`);
  }
}

function loggerOne() {
  const fancyLogger = new FancyLogger();
  fancyLogger.printLogCount();
  fancyLogger.log('First logger');
  fancyLogger.printLogCount();
}

function loggerTwo() {
  const fancyLogger = new FancyLogger();
  fancyLogger.printLogCount();
  fancyLogger.log('Second logger');
  fancyLogger.printLogCount();
}

loggerOne();
loggerTwo();

const fancyLoggerTest1 = new FancyLogger();
const fancyLoggerTest2 = new FancyLogger();
console.log(fancyLoggerTest1 === fancyLoggerTest2);
