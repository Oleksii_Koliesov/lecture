function baseStrategy(amount) {
  return amount;
}

function premiumStrategy(amount) {
  return amount * 0.85;
}

function platinumStrategy(amount) {
  return amount * 0.65;
}

class AutoCart {
  constructor(discount) {
    this.discount = discount;
    this.amount = 0;
  }

  checkout() {
    return this.discount(this.amount);
  }

  setAmount(amount) {
    this.amount = amount;
  }
}

const baseAutoCart = new AutoCart(baseStrategy);
const premiumAutoCart = new AutoCart(premiumStrategy);
const platinumAutoCart = new AutoCart(platinumStrategy);

baseAutoCart.setAmount(50000)
premiumAutoCart.setAmount(50000)
platinumAutoCart.setAmount(50000)

console.log(baseAutoCart.checkout());
console.log(premiumAutoCart.checkout());
console.log(platinumAutoCart.checkout());
